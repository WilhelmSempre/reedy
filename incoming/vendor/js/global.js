
$(function() {

    var beginDate = "August  18, 2015 16:30:00";
	var endDate = "September  10, 2015 00:00:00";

	$('.countdown').final_countdown({
        'start': new Date(beginDate).getTime() / 1000,
        'end': new Date(endDate).getTime() / 1000,
        'now': Date.now() / 1000    
    });
    
    new WOW().init();
 
});
