            <section class="col-xs-12 no-margin no-padding carousel slide rotator" data-ride="carousel">
    			<section class="carousel-inner" role="listbox">
		            <aside class="item text-center active no-margin row no-padding col-xs-12">
                        <img class="img-responsive" src="assets/img/rotator/1.jpg" alt="Strony internetowe">
		                <h1 class="row text-uppercase no-margin fadeInDown animated">Strony internetowe</h1>
		                <h4 class="row fadeInUp animated">Strony WWW projektowane i wdrażane przez nas są wyjątkowe i ponadczasowe!</h4>

		                <a class="more text-uppercase fadeInUp animated" href="">Zobacz nasze realizacje</a>
		            </aside>
		            <aside class="item text-center no-margin row no-padding col-xs-12">
                        <img src="assets/img/rotator/4.jpg" alt="DTP">
		                <h1 class="row text-uppercase no-margin fadeInDown animated">DTP</h1>
		                <h4 class="row fadeInUp animated">Jest wiele dobrych firm oferujących przygotowywanie materiałów do druku. <br>Dzięki naszemu zaangażowaniu i profesjonalizmowi sprawiamy, że jesteśmy najlepsi na rynku!</h4>

		                <a class="more text-uppercase fadeInUp animated" href="">Skorzystaj z naszej oferty DTP</a>
		            </aside>
		            <aside class="item text-center no-margin row no-padding col-xs-12">
                        <img src="assets/img/rotator/3.jpg" alt="Domeny">
		                <h1 class="row text-uppercase no-margin fadeInDown animated">Domeny</h1>
		                <h4 class="row fadeInUp animated">Wybór domeny jest pierwszym i najważniejszym krokiem przy rozpoczęciu działalności w Internecie.<br> Zapewniamy doradztwo i serwis domen!</h4>

		                <a class="more text-uppercase fadeInUp animated" href="">Przejdź do oferty domen</a>
		            </aside>
                    <aside class="item text-center no-margin row no-padding col-xs-12">
                        <img src="assets/img/rotator/5.jpg" alt="Identyfikacja wizualna">
		                <h1 class="row text-uppercase no-margin fadeInDown animated">Identyfikacja wizualna</h1>
		                <h4 class="row fadeInUp animated">Wizerunek w internecie jest bardzo ważnym czynnikiem wpływającym na biznes.<br> Identyfikacja zaprojektowana przez nas sprawi, że Twój biznes będzie się cieszyć popularnością i szacunkiem w Internecie!</h4>

		                <a class="more text-uppercase fadeInUp animated" href="">Zobacz nasze projekty</a>
		            </aside>
                    <aside class="item text-center no-margin row no-padding col-xs-12">
                        <img class="img-responsive" src="assets/img/rotator/2.jpg" alt="Social media">
                        <h1 class="row text-uppercase no-margin fadeInDown animated">Social media</h1>
                        <h4 class="row fadeInUp animated">Social media powodują wzrost popularności Twojego biznesu.<br> Zaufaj nam, a Twój biznes błyskawicznie stanie się widoczny w social media!</h4>

                        <a class="more text-uppercase fadeInUp animated" href="">Zobacz nasze realizacje</a>
                    </aside>
                    <aside class="item text-center no-margin row no-padding col-xs-12">
                        <img class="img-responsive" src="assets/img/rotator/6.jpg" alt="Aplikacje mobilne">
                        <h1 class="row text-uppercase no-margin fadeInDown animated">Aplikacje mobilne</h1>
                        <h4 class="row fadeInUp animated">Masz pomysł na innowacyjną aplikację mobilną?<br> Projektujemy i implementujemy aplikacje na platformę Android. Dzięki możesz spełnić swoje marzenia!</h4>

                        <a class="more text-uppercase fadeInUp animated" href="">Zobacz nasze aplikacje mobilne</a>
                    </aside>
		        </section>
                <section class="pager col-xs-12 no-padding no-margin">
                    <a href="#" rel="nofollow" class="page active text-center text-uppercase col-md-2 col-xs-12">Strony internetowe</a>
                    <a href="#" rel="nofollow" class="page text-center text-uppercase col-md-2 col-xs-12">DTP</a>
                    <a href="#" rel="nofollow" class="page text-center text-uppercase col-md-2 col-xs-12">Domeny</a>
                    <a href="#" rel="nofollow" class="page text-center text-uppercase col-md-2 col-xs-12">Identyfikacja wizualna</a>
                    <a href="#" rel="nofollow" class="page text-center text-uppercase col-md-2 col-xs-12">Social media</a>
                    <a href="#" rel="nofollow" class="page text-center text-uppercase col-md-2 col-xs-12">Aplikacje mobilne</a>
                </section>
            </section>
            <section class="offer col-xs-12 no-padding">
                <section class="container">
                    <h2 class="text-center text-uppercase">Nasza oferta</h2>
                    <p class="text-center">Tworzymy w pełni interaktywne strony internetowe, aplikacje WWW oraz w profesjonalny sposób
                        przygotowujemy materiały do druku. Możesz również u nas zaopatrzyć się w domenę.</p>
                    <aside class="col-xs-12 no-padding list">
                        <aside data-wow-delay="0s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-graphic" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Grafika komputerowa</h4>
                            <p>Stworzymy kompletny zestaw materiałów promocyjnych do druku i publikacji w internecie. Dzieki temu Twoja reklama była dostępna wszędzie i z każdego miejsca.</p>
                            <a href="/?s=offer&id=3" class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.1s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-www" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Strony internetowe</h4>
                            <p>Nasze strony internetowe są wyjątkowe! Posiadają nowoczesny wygląd, przyjazne użytkownikom, dopasowane technicznie, a także w pełni mobilne.</p>
                            <a href="/?s=offer&id=1" class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.2s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-printing" />
                                </svg> 
                            </div>

                            <h4 class="text-uppercase">Poligrafia</h4>
                            <p>Oferujemy druk małoformatowy i wielkoformatowy od wizytówek aż po duże banery. </p>    
                            <a href="/?s=offer&id=2" class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.3s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-advert" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Reklama – Promocja Firmy</h4>
                            <p>Oferujemy kompletną reklamę do pozyskiwania nowych klientów. Używamy niestandardowych metod dzięki czemu promocja firmy jest bardziej skuteczna.</p>
                            <a class="more text-uppercase">Zobacz</a>
                        </aside> 
                        <aside data-wow-delay="0.4s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-domain" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Domeny</h4>
                            <p>Pierwszy krok który pomoże zaistnieć twojemu biznesowi. Pomożemy w wyborze i zakupie Twojej domeny.</p>     
                            <a class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.5s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-cloud" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Cloud - Serwer</h4>
                            <p>Oferujemy wysokiej klasy serwery na których widnieje strona internetowa czy też ważne pliki do których masz dostęp z każdego miejsca na świecie!</p>
                            <a class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.6s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                                <svg class="ico">
                                    <use xlink:href="#shape-social" />
                                </svg>
                            </div>

                            <h4 class="text-uppercase">Social Media</h4>
                            <p>Facebook, Twitter a może Youtube? Właśnie tam Twoi klienci spędzają najwięcej czasu. Przedstawimy im Twoją ofertę najlepiej jak potrafimy.</p>                 
                            <a class="more text-uppercase">Zobacz</a>
                        </aside>
                        <aside data-wow-delay="0.7s" class="item text-center wow fadeInDown col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="svg-wrapper">
                            <svg class="ico">
                                <use xlink:href="#shape-photo" />
                            </svg> 
                            </div>

                            <h4 class="text-uppercase">Fotografia i wideofilmowanie</h4>
                            <p>Wykonujemy profesjonalne zdjęcia do celów reklamowych oraz nagrania wideo. Zapewniamy wysoką jakość naszych zdjęć i nagrań wideo</p>
                            <a class="more text-uppercase">Zobacz</a>
                        </aside>                                       
                    </aside>
                </section>
            </section>
            <section class="portfolio col-xs-12 no-padding">
                <h2 class="text-uppercase text-center">Portfolio</h2>
                <p class="text-center">Poniżej znajdują się prace, które wykonaliśmy.</p>
                <ul class="no-padding text-center category">
                    <li><a rel="nofollow" class="active text-uppercase" data-filter="*" href="#">Wszystkie</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".graphic" href="#">Grafika</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".webdesign" href="#">Strony WWW</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".dtp" href="#">Poligrafia</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".firma" href="#">Reklama</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".promo" href="#">Gadżety</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".social" href="#">Social Media</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".android" href="#">Android</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".scripts" href="#">Skrypty</a></li>
                    <li><a rel="nofollow" class="text-uppercase" data-filter=".wp" href="#">Wordpress</a></li>
                </ul>

                <aside class="list isotope col-xs-12 no-padding">
                    <a href="portfolio/1/index.html"><figure data-category="webdesign" class="webdesign item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>
                    </figure></a>
                    <figure data-category="dtp" class="dtp item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure> 
                    <figure data-category="software" class="software item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure> 
                    <figure data-category="webdesign" class="webdesign item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure>
                    <figure data-category="webdesign" class="webdesign item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure> 
                    <figure data-category="coding" class="coding item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure> 
                    <figure data-category="dtp" class="dtp item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure> 
                    <figure data-category="software" class="software item col-md-3 col-sm-4 col-xs-12 no-padding">
                        <img src="portfolio/1/cover.png" alt="1">

                        <figcaption class="item-description">
                            <h4 class="pull-left text-uppercase no-margin">Reedy</h4>
                            <h5 class="pull-left no-margin">Branding</h5>
                        </figcaption>                            
                    </figure>                               
                </aside>
            </section>
<!--            <section class="domain col-xs-12 no-padding">-->
<!--                <section class="container">-->
<!--                    <h3>Domena dla Ciebie za <strong>0zł</strong>!</h3>-->
<!--                    <p>Biznes zaczyna się od domeny!</p>-->
<!---->
<!--                    <form action="" class="form-inline domain-form col-md-6 col-xs-12 no-padding none">-->
<!--                      <div class="form-group col-md-8 col-xs-12">-->
<!--                        <input class="col-xs-12" name="domain" type="text" placeholder="Wpisz swój adres">-->
<!--                      </div>-->
<!--                      <button class="form-group col-md-4 col-xs-12" type="submit">Sprawdź</button>-->
<!--                    </form>-->
<!--					-->
<!--                    <p class="message ok col-md-6 col-xs-12"></p>-->
<!--                    <p class="domain-price">Domena .pl: <strong>0 PLN</strong>,  Domena .com: <strong>0 PLN</strong>,  Domena .eu: <strong>0 PLN</strong></p>-->
<!--                </section>-->
<!--            </section>-->
            <section class="aboutus col-xs-12 no-padding">
	            <section class="container">
	                <h2 class="text-center text-uppercase">O nas</h2>
	                <p class="text-center">Poznaj nasz zespół</p>
	                
	                <aside class="list text-center col-xs-12 no-padding">
                        <div class="item wow fadeInUp col-md-4 col-xs-12 no-padding">
	                        <!-- <img class="img-circle img-responsive" alt="Łukasz Helman" src="data/team/.jpg"> -->

	                        <h3 class="text-uppercase">Łukasz</h3>
	                        <p class="role col-xs-12 no-margin no-padding">CEO / Social Media / Domain / Servers</p>
	                        <p class="mail col-xs-12 no-margin no-padding">
                                <svg class="ico">
                                    <use xlink:href="#shape-mail-white" />
                                </svg>lukasz@reedy.pl
                            </p>
	                    </div> 
	                    <div class="item text-center wow fadeInUp col-md-4 col-xs-12 no-padding">
	                        <h3 class="text-uppercase">Kamil</h3>
	                        <p class="role col-xs-12 no-margin no-padding">CEO / Marketing / DTP</p>
	                        <p class="mail col-xs-12 no-margin no-padding">
                                <svg class="ico">
                                    <use xlink:href="#shape-mail" />
                                </svg>kamil@reedy.pl
                            </p>
	                    </div>
	                    <div class="item text-center wow fadeInUp col-md-4 col-xs-12 no-padding">

	                        <h3 class="text-uppercase">Piotr</h3>
	                        <p class="role col-xs-12 no-margin no-padding">Photographer</p>
	                        <p class="mail col-xs-12 no-margin no-padding">
                                <svg class="ico">
                                        <use xlink:href="#shape-mail" />
                                    </svg>piotrek@reedy.pl
                                </p>
	                    </div>                         
	                </aside>                                         
	            </section>
            </section> 
            <section class="statistic col-xs-12 no-padding">
                <section class="container">
                    <h2 class="text-center text-uppercase">Kilka faktów o nas</h2>
                    <p class="text-center">Nasze statystyki</p>

                    <aside class="list col-xs-12 no-padding">
                        <div class="item wow fadeInUp col-md-3 col-sm-4 col-xs-12 boxone no-padding text-center">
                            <div data-value="16" class="circle"><strong>0</strong></div><p>tyle godzin dziennie spędzamy przed <strong>komputerem</strong>.</p>
                        </div>
                        <div class="item wow fadeInUp col-md-3 col-sm-4 col-xs-12 boxone no-padding text-center">
                            <div data-value="1000" class="circle"><strong>0</strong></div><p><strong>tyle pomysłów</strong> na minutę dla Twojej firmy!</p>
                        </div>
                        <div class="item wow fadeInUp col-md-3 col-sm-4 col-xs-12 boxone no-padding text-center">
                            <div data-value="9" class="circle"><strong>0</strong></div><p>tyle lat doświadczenia które <strong>rozwijamy</strong> od najmłodszych lat.</p>
                        </div>
                        <div class="item wow fadeInUp col-md-3 col-sm-4 col-xs-12 boxone  no-padding text-center">
                            <div data-value="2" class="circle"><strong>0</strong></div><p>tyle kaw <strong>dziennie</strong> wypija nasza ekipa.</p>
                        </div>
                    </aside>                                       
                </section>
            </section>   
            <section class="contact col-xs-12 no-padding">
                <section class="container">
                    <aside class="address col-md-6 col-xs-12 no-padding">
                        <h2 class="no-margin no-padding text-uppercase">Kontakt</h2>
                        <address>
                            <p class="mail">
                                <svg class="pull-left ico">
                                    <use xlink:href="#shape-mail" />
                                </svg>biuro@reedy.pl
                            </p>
                            <p class="phone">
                                <svg class="pull-left ico">
                                    <use xlink:href="#shape-phone" />
                                </svg>+48 884 310 118
                            </p>
                            <p class="phone">
                                <svg class="pull-left ico">
                                    <use xlink:href="#shape-phone" />
                                </svg>+48 505 404 794
                            </p>
                            <p class="addr">
                                <svg class="pull-left ico">
                                    <use xlink:href="#shape-gps" />
                                </svg>ul. Lwowska 32b, 33-100 Tarnów
                            </p>
                            <p>Nie boimy się sprostać twojemu wyzwaniu. <strong>Napisz do Nas odpowiemy.</strong></p>
                        </address>
                    </aside>
                    <aside class="office col-md-6 col-xs-12 no-padding">
                        <h2 class="no-margin no-padding text-uppercase">Biuro obsługi klienta</h2>
                        <p>Poniedziałek - Piątek: 09:00 - 21:00</p>
                        <p>Sobota: 10:00 - 16:00</p>
                        <p>Niedziela: <strong>nieczynne</strong></p>
                        <p>Zapraszamy do spotkania przy dobrej kawie z duża dozą pomysłów.</p>
                    </aside>
                </section>
            </section>