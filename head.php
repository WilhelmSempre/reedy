<?php
    $request = array_map(function($item) {
        return htmlentities(trim(strip_tags($item)));
    }, $_GET);

    $page = isset($request['s']);
?>
<!doctype html>
<html>
    <head>
    	<meta charset="utf-8">
        <meta name="description" content="Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW Tarnów, Tarnow, Kraków, Krakow, Warszawa">
        <meta property="og:title" content="Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW">
        <meta property="og:description" content="Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW">
        <meta property="og:url" content="http://www.reedy.pl">
        <meta property="og:image" content="">
        <meta property="og:image" content="">
        <meta property="og:type" content="website">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="assets/img/favicon.ico">
        <meta name="description" content="Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW">
        <meta name="keywords" content="Reedy, Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW">
    	<link rel="stylesheet" href="assets/css/dist/style.min.css">
    	<title>Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW</title>
    </head>
    <body>
        <div class="svg-defs"></div>
        <header class="top no-padding col-xs-12">
            <section class="container">
                <p class="mail col-md-2 col-xs-12 no-margin no-padding">
                    <svg class="pull-left ico">
                        <use xlink:href="#shape-mail" />
                    </svg><a href="mailto:biuro@reedy.pl">biuro@reedy.pl</a>
                </p>
                <p class="phone col-md-2 col-xs-12 no-margin no-padding">
                    <svg class="pull-left ico">
                        <use xlink:href="#shape-phone" />
                    </svg>+48 884 310 118
                </p>
                <p class="phone col-md-2 col-xs-12 no-margin no-padding">
                    <svg class="pull-left ico">
                        <use xlink:href="#shape-phone" />
                    </svg>+48 505 404 794
                </p>
                <p class="phone col-md-4 col-xs-12 no-margin no-padding">
                    <svg class="pull-left ico">
                        <use xlink:href="#shape-gps" />
                    </svg>ul. Lwowska 32b, 33-100 Tarnów
                </p>
            </section>
        </header>
        <header class="navigation no-padding col-xs-12">
            <section class="container">
                <a href="/" data-target="" class="pull-left logo"><img alt="Reedy - Grafika komputerowa, Webdesign, Druk, Reklama, Domeny, Cloud, Tworzenie serwisów WWW" src="assets/img/logo.png"></a>
                <nav class="pull-right menu visible-lg visible-md">
                    <ul class="no-padding no-margin">
                        <li><a rel="nofollow" class="text-uppercase" href="/" data-target="">Strona Główna</a></li>
                        <li><a rel="nofollow" class="text-uppercase" href="/#offer" data-target=".offer">Oferta</a></li>
                        <li><a rel="nofollow" class="text-uppercase" href="/#portfolio" data-target=".portfolio">Portfolio</a></li>
                        <li><a rel="nofollow" class="text-uppercase" href="/#aboutus" data-target=".aboutus">O nas</a></li>
                        <li><a rel="nofollow" class="text-uppercase" href="/#contact" data-target=".contact">Kontakt</a></li>
                    </ul>
                </nav>              
            </section>
        </header>
        <main class="col-xs-12 no-padding <?php echo ($page) ? 'page' : 'main'; ?>">
        