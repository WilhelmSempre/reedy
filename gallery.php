<?php
	$gallery = isset($request['id']);

	if ($gallery) {
		$path = 'portfolio/' . $request['id'];
		
		if (file_exists($path . '/index.php')) {
			require_once $path . '/index.php';
		} else {
			require_once '404.php';
		}
	} else {
		require_once '404.php'; 
	}
?>