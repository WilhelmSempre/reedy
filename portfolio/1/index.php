        <section class="portfolio">  
            <section class="container">
                <h3>Reedy</h3>
            </section>
            <div id="wrapper">
                    <div id="carousel">
                        <img src="1.png" alt="Wyzytówka">
                        <img src="2.png" alt="Branding">
                        <img src="3.png" alt="Logotype">
                        <img src="2.png" alt="Branding">
                    </div>
                    <a href="#" id="prev" title="Poprzednie Zdjęcie"> </a>
                    <a href="#" id="next" title="Nastepne Zdjęcie"> </a>
                <div id="pager"></div>
            </div>   
            <section class="container info">
                <p class="title col-xs-12">Reedy - Agencja interaktywna</p>
                <p class="portfolio-category col-xs-12">Branding</p>
                <a class="col-xs-12" href="http://www.reedy.pl">www.reedy.pl</a>
            </section> 
        </section>  