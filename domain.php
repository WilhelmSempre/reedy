<?php

ini_set('display_errors', true);
error_reporting(E_ALL);

/* Clear variables */

$requestData = array_map(function($key) {
	return filter_var(strip_tags(trim($key)), FILTER_SANITIZE_STRING);
}, $_POST);

if (!isset($requestData['domain'])) {
	die('Opps!');
}

if ($requestData['domain'] === '') {
	echo json_encode(array('error' => 4, 'message' => 'Wypełnij pole!'));
	exit();
}

if (!preg_match('#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si', $requestData['domain'])) {
	echo json_encode(array('error' => 2, 'message' => 'Nieprawidłowy adres URL!'));
	exit();
}


require_once dirname(__FILE__) . '/vendor/AvailabilityService.php';

$service = new HelgeSverre\DomainAvailability\AvailabilityService(true);
$available = $service->isAvailable($requestData['domain']);

if ($available) {
	echo json_encode(array('error' => 0, 'message' => "Domena jest dostępna!"));
	exit();
} else {
	echo json_encode(array('error' => 1, 'message' => "Domena jest zajęta!"));
	exit();
}
