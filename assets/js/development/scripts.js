(function($) {

    "use strict";

    var reedy = reedy || {};

    reedy = {

        domain: function () {
            var formDomain = $('form.domain-form');
            var message = $('.domain .message');

            formDomain.on('submit', function (event) {
                var event = event || window.event;

                if (!message.is(':visible')) {
                    message.removeClass('fail').removeClass('ok').removeClass('info');

                    var dataForm = $(this).serialize();

                    $.when($.ajax({
                        type: 'post',
                        dataType: 'json',
                        data: dataForm,
                        url: 'domain.php'
                    })).done(function (response) {
                        if (response.error == 0) {
                            message.addClass('ok').html('<a href="?s=buy">' + response.message + ' <strong>Kliknij aby kupić domenę!</strong></a>');
                        } else if (response.error == 1) {
                            message.addClass('fail').html(response.message);
                        } else {
                            message.addClass('info').html(response.message);
                        }

                        $.when(message.slideDown(200)).done(function () {
                            setTimeout(function () {
                                message.slideUp(200);
                            }, 6000);
                        });
                    });

                }

                if (event.preventDefault()) {
                    event.preventDefault();
                }
            });
        },

        animate: function(navigation, navHeight, top, topHeight) {
            var scrollPosition = $(window).scrollTop(),
                pinNavHeight = parseInt(navigation.css('height')),
                pinTopHeight = parseInt(top.css('height'));

            if (scrollPosition >= 130) {
                if (navigation.is(':visible') && !navigation.is(':animated') && pinNavHeight == navHeight) {
                    navigation.animate({ height : 60, top: 0 }, 200);
                }

                if (top.is(':visible') && !top.is(':animated') && pinTopHeight == topHeight) {
                    top.animate({ height : 0 }, 100);
                }
            } else {
                if (!navigation.is(':animated') && pinNavHeight == 60) {
                    navigation.animate({ height : navHeight, top: topHeight }, 200);
                }

                if (top.is(':visible') && !top.is(':animated')) {
                    top.animate({ height : topHeight }, 100);
                }
            }
        },

        pin: function () {
            var pinNavigation = $("header.navigation"),
                pinTop = $('header.top'),
                pinNavigationHeight = pinNavigation.outerHeight(false),
                pinTopHeight = pinTop.outerHeight(false);

            reedy.animate(pinNavigation, pinNavigationHeight, pinTop, pinTopHeight);

            $(window).on('scroll', function (event) {
                reedy.animate(pinNavigation, pinNavigationHeight, pinTop, pinTopHeight);
            });
        },

        navigation: function () {
            var navigation = $('header nav, footer nav, body header');

            navigation.find('a').on('click', function (event) {
                event = event || window.event;

                var address = location.search;

                if (address == "") {
                    if (event.preventDefault()) {
                        event.preventDefault();
                    }
                }

                var target = $(this).data('target');
                target = (target == '') ? 0 : target;

                $('body').scrollTo(target, 1000, {easing: 'linear'});

            });
        },

        portfolioCategory: function () {

            /* Isotope */
            var isotope = $('.isotope');

            if (isotope.length > 0) {

                var category = isotope.isotope({
                    itemSelector: '.item',
                    layoutMode: 'fitRows'
                });

                var categoryInput = $('.portfolio .category');

                categoryInput.find('a').on('click', function (event) {
                    event = event || window.event;

                    categoryInput.find('a').removeClass('active');

                    var categoryValue = $(this).attr('data-filter');
                    category.isotope({filter: categoryValue});


                    $(this).addClass('active');

                    if (event.preventDefault()) {
                        event.preventDefault();
                    }
                });
            }
        },

        init: function () {

            new WOW().init();

            reedy.portfolioCategory();
            reedy.navigation();
            reedy.pin();
            reedy.domain();
        }
    };

    $(document).ready(function() {

        reedy.init();

        //if (typeof jQuery().carouFredSel != 'undefined') {
        //    $('#carousel').carouFredSel({
        //        width: '100%',
        //        items: {
        //            visible: 3,
        //            start: -1
        //        },
        //        scroll: {
        //            items: 1,
        //            duration: 1000,
        //            timeoutDuration: 3000
        //        },
        //        prev: '#prev',
        //        next: '#next',
        //        pagination: {
        //            container: '#pager',
        //            deviation: 1
        //        }
        //    });
        //}


        var countUpItems = $('.statistic .item h1 .count');
        countUpItems.each(function () {

            var options = {
                useEasing: true,
                useGrouping: true,
                separator: '.',
                decimal: '.',
                prefix: '',
                suffix: ''
            };

            var countUpItemsValue = parseInt($(this).text()),
                countUpInstance = new CountUp($(this), 0, countUpItemsValue, 0, 2.5, options);

            countUpInstance.start();
        });

        $('.circle').circleProgress({
            size: 150,
            thickness: 10,
            emptyFill: "transparent",
            lineCap: "round",
            fill: {
                color: "#e74766"
            }
        }).on('circle-animation-progress', function(event, progress, stepValue) {
            $(this).find('strong').text(parseInt(stepValue));
        });


        if (location.hash != '') {
            var page = '.' + location.hash.slice(1);
            $('body').scrollTo(page, 1000, {easing: 'linear'});
        }

        $('.rotator').on('slide.bs.carousel', function() {
            $(this).find('h2').hide().removeClass('fadeInDown animated');
            $(this).find('h4').hide().removeClass('fadeInUp animated');
            $(this).find('.more').hide().removeClass('fadeInUp animated');
        }).on('slid.bs.carousel', function(event) {
            var index = $(this).find('.item.active').index(),
                activePage = $(this).find('.pager .page.active');
                page = $(this).find('.pager .page');

            activePage.removeClass('active');
            page.eq(index).addClass('active');

            $(this).find('h2').show().addClass('fadeInDown animated');
            $(this).find('h4').show().addClass('fadeInUp animated');
            $(this).find('.more').show().addClass('fadeInUp animated');

        }).find('.pager .page').on('click', function(event) {
            event = event || window.event;

            var index = $(this).index();

            $('.rotator').carousel(index);

            if (event.preventDefault()) {
                event.preventDefault();
            }
        });

        $('.tablesorter').tablesorter({
            sortList: [[0, 0], [1, 0]],
            widgets: [
                "zebra",
                "filter"
            ],
            debug: true
        });

        $('select').selectric();
    });

})(jQuery);