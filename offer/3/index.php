<section class="container offers">
    <aside class="pull-left cover col-md-4 no-margin no-padding">
        <img class="img-responsive" src="offer/3/photo.jpg" alt="Grafika komputerowa">
    </aside>
    <aside class="pull-right info col-md-8 no-margin no-padding">
        <h3 class="text-center">Grafika komputerowa</h3>
        <p class="description no-margin no-padding">
            Identyfikacja wizualna<br><br>

            Sporządzimy dla Twojeje firmy kompletny tzw. brading internetowy w który wskład wchodzi Logotypy, bannery
            animowane, katalogi, gazetki i nie tylko wszystko wdrążymy do internetu, aby twoja reklama była dostępna
            wszędzie i z każego miejsca.
            <br><br>
            <strong>Zadbaj z nami o swoją firmę, uzyskaj konsumentów, a zyskasz wysokie słupki sprzedaży i pozycję
                lidera wśród konkurencji.</strong>
        </p>
    </aside>
    <aside class="price col-xs-12 no-margin no-padding">
        <h3 class="text-uppercase">Cennik
            <small>Grafika komputerowa</small>
        </h3>
        <div class="table-responsive">
            <table class="table tablesorter pricelist col-xs-12">
                <thead>
                <tr>
                    <th class="filter-false">Opis</th>
                    <th class="filter-false">Cena</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Projekt logotypu</td>
                    <td>200zł - 1000zł</td>
                </tr>
                <tr>
                    <td>Bannery animowane</td>
                    <td>50zł - 200zł</td>
                </tr>
                <tr>
                    <td>Katalogi</td>
                    <td>ok. 300zł + 20zł za każda podstronę</td>
                </tr>
                <tr>
                    <td>Projekt Plakatu</td>
                    <td>ok. 80zł</td>
                </tr>
                <tr>
                    <td>Projekt Teczki</td>
                    <td>ok. 80zł</td>
                </tr>
                <tr>
                    <td>Gazetki</td>
                    <td>ok. 300zł + 20zł za każda podstronę</td>
                </tr>
                </tbody>
            </table>
        </div>
    </aside>
</section>

	
	