<section class="container offers">
    <aside class="left cover col-md-4 no-margin no-padding">
        <img class="img-responsive" src="offer/2/photo.jpg" alt="Poligrafia">
    </aside>
    <aside class="right info col-md-8 no-margin no-padding">
        <h3 class="text-uppercase">Poligrafia</h3>
        <p class="description no-margin no-padding">
            Druk wielkoforamtowy i wielocyfrowy<br><br>

            Sporządzimy dla Twojeje firmy kompletny tzw. brading w który wskład wchodzi logotyp, ulotki, wizytówki,
            papier firmowy, pięczątki, teczki, foldery, banery, boillboardy, tablice, reklame na pojazdach, witrnyach
            sklepowych, elewacjach budynków,
            gadzety reklamowe ( długopisy, kubki, ) i nie tylko. Dzięki nam możesz pokazaćsie i przypomnieć klientom na
            wiele sposobów, oraz sprawić że Twoja firma będzie dobrze zapamiętana.
            <br><br>
            <strong>Zadbaj z nami o swoją firmę, uzyskaj konsumentów, a zyskasz wysokie słupki sprzedaży i pozycję
                lidera wśród konkurencji.</strong>
        </p>
    </aside>
    <ul role="tablist" class="tabs col-xs-12 no-margin no-padding list-unstyled">
        <li role="presentation" class="tab active col-md-3 col-xs-12 text-uppercase text-center">
            <a rel="nofollow" aria-controls="branding" role="tab" data-toggle="tab" href="#branding">Branding</a>
        </li>
        <li class="tab col-md-3 col-xs-12 text-uppercase text-center">
            <a rel="nofollow" aria-controls="vise" role="tab" data-toggle="tab" href="#vise">Wizytówki</a>
        </li>
        <li class="tab col-md-3 col-xs-12 text-uppercase text-center">
            <a rel="nofollow" aria-controls="folders" role="tab" data-toggle="tab" href="#folders">Ulotki</a>
        </li>
        <li class="tab col-md-3 col-xs-12 text-uppercase text-center">
            <a rel="nofollow" aria-controls="folders-x" role="tab" data-toggle="tab" href="#folders-x">Ulotki składane</a>
        </li>
    </ul>
    <section class="tab-content">
        <aside id="branding" role="tabpanel" class="tab-pane fade in active branding price col-xs-12 no-margin no-padding">
            <h3 class="text-uppercase">Cennik
                <small>Branding</small>
            </h3>
            <div class="table-responsive">
                <table class="table tablesorter pricelist col-xs-12">
                    <thead>
                    <tr>
                        <th class="filter-false">Typ usługi</th>
                        <th class="filter-false">Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Projekt Logotypu</td>
                        <td>200zł - 1000zł</td>
                    </tr>
                    <tr>
                        <td>Projekt Wizytówek</td>
                        <td>50zł - 200zł</td>
                    </tr>
                    <tr>
                        <td>Projekt Ulotek</td>
                        <td>50zł - 300zł</td>
                    </tr>
                    <tr>
                        <td>Projekt Plakatu</td>
                        <td>ok. 80zł</td>
                    </tr>
                    <tr>
                        <td>Projekt Teczki</td>
                        <td>ok. 80zł</td>
                    </tr>
                    <tr>
                        <td>Projekt Folderu</td>
                        <td>ok. 300zł + 20zł za każda podstronę</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </aside>
        <aside id="vise" role="tabpanel" class="tab-pane fade vise price col-xs-12 no-margin no-padding">
            <h3 class="text-uppercase">Cennik
                <small>Wizytówki</small>
            </h3>
            <div class="table-responsive">
                <table class="table tablesorter pricelist col-xs-12">
                    <thead>
                    <tr class="head">
                        <th data-placeholder="Wybierz format" class="filter-select">Format</th>
                        <th class="filter-false">Ilość</th>
                        <th data-placeholder="Wybierz rodzaj papieru" class="filter-select">Rodzaj papieru</th>
                        <th data-placeholder="Wybierz zadruk" class="filter-select">Zadruk</th>
                        <th class="filter-false">Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>100</td>
                        <td>350g</td>
                        <td>4+0 jednostronny</td>
                        <td>18zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>200</td>
                        <td>350g</td>
                        <td>4+0 jednostronny</td>
                        <td>28zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>500</td>
                        <td>350g</td>
                        <td>4+0 jednostronny</td>
                        <td>50zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>1000</td>
                        <td>350g</td>
                        <td>4+0 jednostronny</td>
                        <td>80zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>100</td>
                        <td>350g</td>
                        <td>4+4 dwustronny</td>
                        <td>28zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>200</td>
                        <td>350g</td>
                        <td>4+4 dwustronny</td>
                        <td>39zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>500</td>
                        <td>350g</td>
                        <td>4+4 dwustronny</td>
                        <td>75zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>1000</td>
                        <td>350g</td>
                        <td>4+4 dwustronny</td>
                        <td>80zł</td>
                    </tr>
                    <tr>
                        <td>Wizytówki 90x50</td>
                        <td>10.000</td>
                        <td>350g</td>
                        <td>4+4 dwustronny</td>
                        <td>550zł</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </aside>
        <aside id="folders" role="tabpanel" class="folders tab-pane fade price col-xs-12 no-margin no-padding">
            <h3 class="text-uppercase">Cennik
                <small>Ulotki</small>
            </h3>
            <div class="table-responsive">
                <table class="table tablesorter pricelist col-xs-12">
                    <thead>
                    <tr class="head">
                        <th data-placeholder="Wybierz format" class="filter-select">Format</th>
                        <th class="filter-false">Ilość</th>
                        <th data-placeholder="Wybierz rodzaj papieru" class="filter-select">Rodzaj papieru</th>
                        <th data-placeholder="Wybierz zadruk" class="filter-select">Zadruk</th>
                        <th class="filter-false">Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>100</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>49zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>88zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>88zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>97zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>250</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>139zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>99zł</td>
                    </tr>
                    <tr>
                        <td>DL (99x210mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>109zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>140zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>165zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>190zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>140zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>165zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>190zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>164zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>181zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>211zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>164zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>181zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>211zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>168zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>203zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>242zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>345zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>250</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>203zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>500</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>242zł</td>
                    </tr>
                    <tr>
                        <td>2xDL (198x210mm)</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>345zł</td>
                    </tr>
                    <tr>
                        <td>A5 (148x210mm)</td>
                        <td>100</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>78zł</td>
                    </tr>
                    <tr>
                        <td>A5 (148x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>119zł</td>
                    </tr>
                    <tr>
                        <td>A5 (148x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>133zł</td>
                    </tr>
                    <tr>
                        <td>A5 (148x210mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>169zł</td>
                    </tr>
                    <tr>
                        <td>A5 (148x210mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>148zł</td>
                    </tr>
                    <tr>
                        <td>A6 (105x148mm)</td>
                        <td>100</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>64zł</td>
                    </tr>
                    <tr>
                        <td>A6 (105x148mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>72zł</td>
                    </tr>
                    <tr>
                        <td>A6 (105x148mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>79zł</td>
                    </tr>
                    <tr>
                        <td>A6 (105x148mm)</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>99zł</td>
                    </tr>
                    <tr>
                        <td>A6 (105x148mm)</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>142zł</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </aside>
        <aside id="folders-x" role="tabpanel" class="price tab-pane fade folders-x col-xs-12 no-margin no-padding">
            <h3 class="text-uppercase">Cennik
                <small>Ulotki Składane</small>
            </h3>
            <div class="table-responsive">
                <table class="table tablesorter pricelist col-xs-12">
                    <thead>
                    <tr class="head">
                        <th data-placeholder="Wybierz format" class="filter-select">Format</th>
                        <th class="filter-false">Ilość</th>
                        <th data-placeholder="Wybierz rodzaj papieru" class="filter-select">Rodzaj papieru</th>
                        <th data-placeholder="Wybierz zadruk" class="filter-select">Zadruk</th>
                        <th class="filter-false">Cena</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>107zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>127zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>146zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>107zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>127zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>146zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>142zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>149zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>155zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>142zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>149zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>155zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>147zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>180zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>187zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>212zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>250</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>180zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>500</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>187zł</td>
                    </tr>
                    <tr>
                        <td>A6 do A7</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>212zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>132zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>146zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>169zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>132zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>146zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>169zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>149zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>154zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>186zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>149zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>154zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>186zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>200zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>287zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>290zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>380zł</td>
                    </tr>

                    <tr>
                        <td>A5 do A6</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>287zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>290zł</td>
                    </tr>
                    <tr>
                        <td>A5 do A6</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>380zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>198zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>226zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>266zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>198zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>226zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>266zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>254zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>295zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>254zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>295zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>233zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>296zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>

                    <tr>
                        <td>A4 do A5</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>296zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>
                    <tr>
                        <td>A4 do A5</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>372zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>439zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>487zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>372zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>439zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>487zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>387zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>517zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>387zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>517zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>385zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>572zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>621zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>759zł</td>
                    </tr>

                    <tr>
                        <td>A3 do A4</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>572zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>621zł</td>
                    </tr>
                    <tr>
                        <td>A3 do A4</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>759zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>198zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>226zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>266zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>198zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>226zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>266zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>254zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>295zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>254zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>295zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>233zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>296zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>

                    <tr>
                        <td>A4 do 105/297</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>296zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>
                    <tr>
                        <td>A4 do 105/297</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>444zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>160zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>179zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda błysk 130g</td>
                        <td>4+4 dwustronny</td>
                        <td>211zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>160zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>179zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>211zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>173zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>196zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda błysk 170g</td>
                        <td>4+4 dwustronny</td>
                        <td>235zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>173zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>196zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda mat 170g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>235zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda błysk 250g</td>
                        <td>4+4 dwustronny</td>
                        <td>187zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>273zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda błysk 300g</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>

                    <tr>
                        <td>2xDL do DL</td>
                        <td>250</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>221zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>500</td>
                        <td>Kreda mat 130g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>273zł</td>
                    </tr>
                    <tr>
                        <td>2xDL do DL</td>
                        <td>1000</td>
                        <td>Kreda mat 300g + lakier dyspersyjny</td>
                        <td>4+4 dwustronny</td>
                        <td>369zł</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </aside>
    </section>
</section>

	
	