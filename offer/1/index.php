<section class="container offers">
    <aside class="pull-left cover col-md-4 col-xs-12 no-margin no-padding">
        <img class="img-responsive" src="offer/1/photo.jpg" alt="Strony internetowe">
    </aside>
    <aside class="pull-right info col-md-8 col-xs-12 no-margin no-padding">
        <h3 class="text-uppercase">Strony internetowe</h3>
        <p class="description no-margin no-padding">
            Dobra strona to podstawa.<br><br>

            Tworzymy nowoczesne strony internetowe, jak również modyfikujemy i ulepszamy już istniejące.
            Nowoczesny wygląd tzw. flat, który z dnia na dzień zyskuje coraz większą popularność można dostrzec na
            przykładzie m.in Google, Otomoto czy innych znanych portalach. Dodatkowo nasze strony są przyjazne
            użytkownikom, gdyż posiadają intuicyjny interfejs. Są również przyjazne użytkownikom mobilnym.
            Dodatkowo ważna również kwestią jest Google. Z wyszukiwarki internetowej korzystamy praktycznie codziennie.
            Aby zaistnieć w wyszukiwarce nie wystarczy jedynie posiadać strony internowej. Twoja witryna musi spełniać
            ważne wymogi, które zapewniamy przez co Twoja strona może być w szybkim czasie na czele w wynikach
            wyszukiwania.
            <br><br>
            <strong>Zadbaj z nami o swoją firmę, uzyskaj konsumentów, a zyskasz wysokie słupki sprzedaży i pozycję
                lidera wśród konkurencji.</strong>
        </p>
    </aside>
    <aside class="web col-xs-12 no-margin no-padding">
        <h3 class="text-uppercase">Cennik
            <small>Strony internetowe</small></h3>
        <table class="table tablesorter pricelist col-xs-12">
            <thead>
            <tr>
                <th class="filter-false">Opis</th>
                <th class="filter-false">Cena</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Projekt graficzny strony tzw. layout</td>
                <td>ok. 299PLN</td>
            </tr>
            <tr>
                <td>Projekt podstron</td>
                <td>ok. 99PLN</td>
            </tr>
            <tr>
                <td>Implementacja projektu</td>
                <td>ok. 499PLN</td>
            </tr>
            <tr>
                <td>Formularz kontaktowy</td>
                <td>ok. 99PLN</td>
            </tr>
            <tr>
                <td>Galeria zdjęć</td>
                <td>ok. 49PLN</td>
            </tr>
            <tr>
                <td>Minimalistyczna wersja mobilna</td>
                <td>ok. 249PLN</td>
            </tr>
            <tr>
                <td>Pomoc techniczna do 48h</td>
                <td>ok. 99PLN</td>
            </tr>
            <tr>
                <td><strong>Podsumowanie</strong></td>
                <td><strong>ok. 1299PLN</strong></td>
            </tr>
            </tbody>
        </table>
    </aside>
</section>
<section class="packets col-xs-12 no-padding">
    <section class="container">
        <p class="col-xs-12">Korzystając z naszych usług masz możliwość zamówienia strony WWW w trzech pakietach.</p>
        <aside class="packet col-md-4 col-xs-12">
            <div class="packet-wrapper">
                <h2 class="text-uppercase">Silver</h2>
                <ul class="no-padding list-unstyled">
                    <li>Indywidualny projekt graficzny;</li>
                    <li>Kodowanie;</li>
                    <li>Galeria, Kontakt;</li>
                    <li>Social media (Facebook)</li>
                </ul>

                <p class="packet-price text-center">1000 PLN (z VAT)</p>
            </div>
        </aside>
        <aside class="packet col-md-4 col-xs-12">
            <div class="packet-wrapper">
                <h2 class="text-uppercase">Gold</h2>
                <ul class="no-padding list-unstyled">
                    <li>Indywidualny projekt graficzny;</li>
                    <li>Kodowanie;</li>
                    <li>Galeria, Kontakt (Mapa, Formularz);</li>
                    <li>Social media (Facebook);</li>
                    <li>Optymalizacja SEO</li>
                </ul>

                <p class="packet-price text-center">1500 PLN (z VAT)</p>
            </div>
        </aside>
        <aside class="packet col-md-4 col-xs-12">
            <div class="packet-wrapper">
                <h2 class="text-uppercase">Platinium</h2>
                <ul class="no-padding list-unstyled">
                    <li>Indywidualny projekt graficzny;</li>
                    <li>Kodowanie;</li>
                    <li>Galeria, Kontakt (Mapa, Formularz);</li>
                    <li>System CMS (Wordpress);</li>
                    <li>Social media (Facebook);</li>
                    <li>Optymalizacja SEO;</li>
                    <li>Własna domena</li>
                </ul>

                <p class="packet-price text-center">2000 PLN (z VAT)</p>
            </div>
        </aside>
    </section>
</section>

	
	