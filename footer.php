            <footer class="col-xs-12 no-padding">
                <section class="container">
                    <nav class="pull-left col-xs-12 no-padding">
                        <ul class="no-padding">
                            <li><a rel="nofollow" class="text-uppercase" href="/" data-target="">Strona głowna</a></li>
                            <li><a rel="nofollow" class="text-uppercase" href="/#offer" data-target=".offer">Oferta</a></li>
                            <li><a rel="nofollow" class="text-uppercase" href="/#portfolio" data-target=".portfolio">Portfolio</a></li>
                            <li><a rel="nofollow" class="text-uppercase" href="/#aboutus" data-target=".aboutus">O nas</a></li>
                            <li><a rel="nofollow" class="text-uppercase" href="/#contact" data-target=".contact">Kontakt</a></li>
                        </ul>
                    </nav>
                    <p class="copyright text-center col-xs-12 no-padding">
                        Reedy.pl &copy; Wszelkie prawa zastrzeżone, kopiowanie materiałów zawartych na stronie bez uprzedniej zgody właściciela jest zabronione.
                    </p>
                </section>
            </footer>    
        </main>
        <script async src="assets/js/dist/main.min.js"></script>
    </body>
</html>