<?php require_once 'head.php'; ?>
<?php 

	if ($page) {
		if (file_exists($request['s'] . '.php')) {
			require_once $request['s'] . '.php';
		} else {
			require_once '404.php';
		}
	} else {
		require_once 'home.php'; 
	}
?>
<?php require_once 'footer.php'; ?>