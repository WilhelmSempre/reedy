<?php
	$offer = isset($request['id']);
	$path = 'offer/' . $request['id'];

	if ($offer) {
		if (file_exists($path . '/index.php')) {
			require_once $path . '/index.php';
		} else {
			require_once '404.php';
		}
	} else {
		require_once '404.php'; 
	}
?>