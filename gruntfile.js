module.exports = function (grunt) {

    var mozjpeg = require('imagemin-mozjpeg');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            build: {
                src: 'assets/js/development/main.js',
                dest: 'assets/js/dist/main.min.js'
            }
        },
        jshint: {
            all: {
                src: ['assets/js/*.js']
            }
        },
        concat: {
            options: {
                separator: ';',
                processImport: false
            },
            dist: {
                src: ['assets/js/development/bower.js', 'assets/js/development/scripts.js', 'assets/js/development/icons.js'],
                dest: 'assets/js/development/main.js'
            }
        },
        cssmin: {
            target: {
                files: {
                    'assets/css/dist/style.min.css': [
                        'assets/css/development/*.css'
                    ]
                }
            }
        },
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg()]
                },
                files: {
                    'img/*.png': 'img/*.png',
                    'img/*.jpg': 'img/*.jpg',
                    'img/*.gif': 'img/*.gif'
                }
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    noCache: true
                },
                files: [{
                    expand: true,
                    cwd: 'assets/scss',
                    src: ['*.scss'],
                    dest: 'assets/css/development',
                    ext: '.css'
                }]
            }
        },
        svgstore: {
            options: {
                prefix: 'shape-',
                cleanup: true,
                convertNameToId: function(name) {
                    return name.replace(/^\w+\_/, '');
                }
            },
            default: {
                files: {
                    'assets/img/icons/dist/icons.svg': ['assets/img/icons/*.svg']
                }
            }
        },
        svginjector: {
            icons: {
                files: {
                    'assets/js/development/icons.js': ['assets/img/icons/dist/icons.svg']
                },
                options: {
                    container: '.svg-defs'
                }
            }
        },
        bower_concat: {
            all: {
                options: {
                    separator: ';'
                },
                dest: {
                    'js': 'assets/js/development/bower.js',
                    'css': 'assets/css/development/bower.css'
                },
                dependencies: {
                    'bootstrap': 'jquery',
                    'jquery-ui': 'jquery',
                    'jquery-backstretch': 'jquery',
                    'isotope': 'jquery',
                    'jquery.scrollTo': 'jquery',
                    'sweetalert' : 'jquery',
                    'tablesorter' : 'jquery'
                },
                exclude: [
                    'fizzy-ui-utils',
                    'get-size',
                    'get-style-property',
                    'masonry',
                    'matches-selector',
                    'outlayer'
                ],
                mainFiles: {
                    'bootstrap': ['dist/css/bootstrap.css', 'dist/js/bootstrap.js'],
                    'isotope' : 'dist/isotope.pkgd.js',
                    'tablesorter': ['dist/js/jquery.tablesorter.combined.js', 'dist/js/jquery.tablesorter.widgets.js']
                },
                bowerOptions: {
                    relative: false
                }
            }
        },
        watch: {
            scripts: {
                files: [
                    'gruntfile.js',
                    'assets/js/development/*.js',
                    'assets/css/development/*.css',
                    'package.json',
                    'assets/scss/*.scss'],
                tasks: [
                    'jshint',
                    'bower_concat',
                    'sass',
                    'svgstore',
                    'svginjector',
                    'concat',
                    'uglify',
                    'cssmin'
                ]
            },
            options: {
                dateFormat: function (time) {
                    grunt.log.writeln('Monitorowanie zakończone w ciągu: ' + time + 'ms o godzinie: ' + (new Date()).toString());
                    grunt.log.writeln('Monitorowanie plików');
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-bower-concat');
    grunt.loadNpmTasks('grunt-svginjector');

    grunt.registerTask('default', [
        'jshint',
        'bower_concat',
        'sass',
        'svgstore',
        'svginjector',
        'concat',
        'uglify',
        'cssmin'
    ]);

    grunt.registerTask('image', [
        'imagemin'
    ], function () {
        grunt.log.writeln('Optymalizacja i kompresja obrazków!');
    });

};